# Q4 Planning: Backend Systems
Streamline cleaning and development of data pipelines, CSTEP data migration, Data dictionary and upgrade to Airflow 2.X

## 1. Restructure Airflow Repo (24 hrs total / 4 weeks to complete)
Organize the Airflow repo to for better workflow and streamlining the development process. Also, implement an automated process of pulling SQL scripts from a separate repo.

- **1.1 (16 hrs / 2 weeks):** Re-organize Airflow repo
    - Separate files into three main folders: Dags, Automation and SQL
    - Move function_/classes_ files into appropriate dag folder instead of the root dag folder.
- **1.2 (8hrs / 2 weeks):** Automate pulling of SQL scripts
    - Set up process to pull scripts from separate repo where the SQL scripts will live.
- **1.3: Stretch Goals:**
    - **(Stretch Goal)** Reformat code to follow PEP8. Implement linter, black or flake8 to reformat code upon git commit.
    - **(Stretch Goal)** Add documentation to widely used classes & functions.
---
## 2. Migrate Airflow to 2.X (40-48 hrs total / 4-6 weeks)
Current Airflow version is 1.10.6, and current Python version is 3.6.14. Airflow and Python need to be upgraded to the most current version, and a proces should be developed for updating systems in the future to stay up-to-date.

- **2.1 (8-12 hrs / 1 week)**: Scope Airflow 2.X and migration
    - Find out pitfalls of updating from older version
    - Determine how to update docker file for deployment
    - Ensure all external modules and depencies will work with new Python and Airflow versions
    - Research new features
- **2.2 (16 hrs / 1-2 weeks):** Implementing Updates
    - Change code base and dockerfiles to make updates, update python, external modules, etc.
- **2.3 (1-3 Weeks):** Testing 
    - Test existing code base with new python and airflow versions and update as necessary.
---
## 3. Migrate CSTEP Data and Decouple Sources (48-64 hrs / 6-8 weeks)
There are several tables living in the CSTEP database that need to be migrated to new location.

- **3.1 (8hrs / 1 week):** Scoping / Identify all tables to be moved w/ DE and divide into batches
- **3.2 (40-56hrs / 5-6 weeks)**: Change Airflow code to write to new tables
---
## 4. Create Data Dictionary (32-40 hrs / 4-6 weeks)
Every object that we pull from APIs should be defined. Start with defining the most used fields.

- **4.1 (16-20 hrs / 2-3 weeks):** Identify most used fields and define them
- **4.2 (16-20 hrs / 2-3 weeks):** Define the remaining fields
---

# Sprint Planning

### Sprint 1 (10/11/2021):

- [ ] 1.1 Airflow Repo: Re-organize Airflow repo
- [ ] 3.1 CSTEP Migration: Scope table migration
---
### Sprint 2 (10/25/2021):

- [ ] 1.2 Airflow Repo: Automate pulling of SQL scripts
- [ ] 3.2 CSTEP Migration: Change Airflow code for new table location
- [ ] 4.1 Data Dictionary: Define important fields
---
### Sprint 3 (11/08/2021):

- [ ] 2.1 Airflow 2.X: Scoping
- [ ] 3.2 CSTEP Migration: Change Airflow code for new table location
- [ ] 4.1 Data Dictionary: (carry-over)
- [ ] 4.2 Data Dictionary: Define remaining fields
---
### Sprint 4 (11/22/2021):

- [ ] 2.2 Airflow 2.X: Update codebase
- [ ] 2.3 Airflow 2.X: Testing
- [ ] 3.2 CSTEP Migration: Change Airflow code for new table location
- [ ] 4.2 Data Dictionary: (carry-over)
---
### Sprint 5 (12/06/2021):

- [ ] 1.3 Airflow Repo: Stretch goals
- [ ] 2.3 Airflow 2.X: Testing
- [ ] 3.2 CSTEP Migration: Change Airflow code for new table location (carry-over)

---
### Sprint 6 (12/20/2021):

- [ ] 1.3 Airflow Repo: Stretch goals
- [ ] 2.3 Airflow 2.X testing (overflow)
- [ ] 3.2 CSTEP Migration: Change Airflow code for new table location (carry-over)


